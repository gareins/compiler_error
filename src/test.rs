use ast::*;
use arithmetic::*;

//#[test]
//fn arithmetics()
//{
//    assert_eq!(expression("1+1"),
//               Ok(Expression::Sum(Box::new(Expression::Atom(Atom::Integer(1))),
//                                  Box::new(Expression::Atom(Atom::Integer(1))))));
//    assert_eq!(expression("5*5"),
//               Ok(Expression::Product(Box::new(Expression::Atom(Atom::Integer(5))),
//                                      Box::new(Expression::Atom(Atom::Integer(5))))));
//    assert_eq!(expression("2+3*4"),
//               Ok(Expression::Sum(Box::new(Expression::Atom(Atom::Integer(2))),
//                                  Box::new(Expression::Product(Box::new(Expression::Atom(Atom::Integer(3))),
//                                                               Box::new(Expression::Atom(Atom::Integer(4))))))));
//    assert!(expression("(22+)+1").is_err());
//    assert!(expression("1++1").is_err());
//    assert!(expression("3)+1").is_err());
//}
//

#[test]
fn atoms_identificators()
{
    assert_eq!(identificator("a"), Ok(String::from("a")));
    assert_eq!(identificator("_asd"), Ok(String::from("_asd")));
    assert_eq!(identificator("as_d123"), Ok(String::from("as_d123")));

    assert!(identificator("as-d123").is_err());
    assert!(identificator("0a").is_err());
}

#[test]
fn atoms_integer()
{
    assert_eq!(signed("123"), Ok(123));
    assert_eq!(signed("-123"), Ok(-123));
    assert_eq!(signed("- 123"), Ok(-123));
    assert_eq!(signed("-    123"), Ok(-123));
    assert_eq!(signed("00001"), Ok(1));
}

#[test]
fn atoms_decimal()
{
    assert!(decimal("123").is_err());
    assert_eq!(decimal("-123.0"), Ok(-123.0));
    assert_eq!(decimal("+123.0"), Ok(123.0));
    assert_eq!(decimal("00123.91"), Ok(123.91));
}

#[test]
fn atoms_exp()
{
    assert!(exponential("123e1.0").is_err());
    assert_eq!(exponential("123.0e1"), Ok(1230.0));
    assert_eq!(exponential("123.0e0"), Ok(123.0));

    assert_eq!(exponential("-123.0e0"), Ok(-123.0));
    assert_eq!(exponential("-123.0e-1"), Ok(-12.3));
}

#[test]
fn atoms_hex()
{
    assert!(exponential("0x1t").is_err());
    assert_eq!(hexadecimal("0x09a4"), Ok(2468));
    assert_eq!(hexadecimal("-0x09a4"), Ok(-2468));
}

#[test]
fn atoms()
{
    assert_eq!(atom("123"),         Ok(Atom::Integer(123)));
    assert_eq!(atom("-123.1"),      Ok(Atom::Float(-123.1)));
    assert_eq!(atom("- 0x09a4"),    Ok(Atom::Integer(-2468)));
    assert_eq!(atom("-13.0e-1"),    Ok(Atom::Float(-1.3)));
    assert_eq!(atom("as_d123"),     Ok(Atom::Identificator(String::from("as_d123"))));
    assert_eq!(atom("[1 2, 3]"),    Ok(Atom::Vector(vec![Atom::Integer(1), Atom::Integer(2), Atom::Integer(3)])));
    assert_eq!(atom("[1 2;3,4]"),   Ok(Atom::Matrix(vec![vec![Atom::Integer(1), Atom::Integer(2)], vec![Atom::Integer(3), Atom::Integer(4)]])));
    assert_eq!(atom("gcd(100,15)"), Ok(Atom::Call(BracketOperator{ name: Box::new(Atom::Identificator(String::from("gcd"))),
                                                                   args: vec![Atom::Integer(100), Atom::Integer(15)] })));

    assert!(atom("0x1t").is_err());
}

#[test]
fn atom_vector()
{
    let result = Ok(vec![Atom::Integer(1), Atom::Integer(2), Atom::Float(1.3)]);
    assert_eq!(_vector("[1 2 1.3]"),  result);
    assert_eq!(_vector("[1,2,1.3]"),  result);
    assert_eq!(_vector("[   1   2  , 1.3 ]"),  result);

    let result = Ok(vec![Atom::Integer(1), Atom::Integer(-17), Atom::Float(1e-3)]);
    assert_eq!(_vector("[1 -0x11 0.001]"),  result);
    assert_eq!(_vector("[1 -0x11 1e-3]"),  result);
    assert_eq!(_vector(
    "[
    1 -0x11 1e-3
    ]"),  result);
}

#[test]
fn atom_matrix()
{
    let result = Ok(vec![vec![Atom::Integer(1), Atom::Integer(2), Atom::Float(1.3)],
                         vec![Atom::Integer(-1), Atom::Float(2.5), Atom::Float(0.001)]]);

    assert_eq!(_matrix("[1 2 1.3;-1 2.5 0.001]"),  result);
    assert_eq!(_matrix(
    "[1 2 1.3
    -1 2.5 0.001]"),  result);
    assert_eq!(_matrix(
    "[1 2 1.3; -1 2.5 0.001
    ]"),  result);
    assert_eq!(_matrix(
    "[

    1 2 1.3

     -1 2.5 0.001


    ]"),  result);
    assert_eq!(_matrix(
    "[1, 2 1.3
    -1 2.5, 0.001]"),  result);
}

#[test]
fn atom_bracket()
{
    let result = Ok(BracketOperator { name:Box::new(Atom::Identificator(String::from("bla"))), args:Vec::new() });
    assert_eq!(bracketOperation("bla()"), result);
    assert_eq!(bracketOperation("bla ()"), result);
    assert_eq!(bracketOperation("bla(  )"), result);
    assert_eq!(bracketOperation("bla    (   )"), result);

    let result = Ok(BracketOperator { name:Box::new(Atom::Identificator(String::from("bla"))),
                                      args:vec![Atom::Identificator(String::from("a")),
                                                Atom::Vector(vec![Atom::Integer(1), Atom::Float(0.3)])]});
    assert_eq!(bracketOperation("bla(a, [1 0.3])"), result);
}

#[test]
fn atom_index()
{
    let result = Ok(IndexOperator {..Default::default() });
    assert_eq!(index("0:1"), result);
    assert_eq!(index("0:1").unwrap().full, false);
    assert_eq!(index("0:1:1"), result);
    assert_eq!(index("0 :   1:  1"), result);

    let result = Ok(IndexOperator {to: Box::new(Atom::Integer(3)), delta: Box::new(Atom::Float(0.1)), ..Default::default() });
    assert_eq!(index("0:0.1:3"), result);

    let result = Ok(IndexOperator {full: true, ..Default::default() });
    assert_eq!(index(":"), result);
}
