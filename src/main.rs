#![feature(plugin)]
#![plugin(peg_syntax_ext)]

mod ast;
mod test;

peg_file! arithmetic("grammar.rustpeg");

fn main() {
	println!("Hello world!");
}
