use ast::*;
use std::i64;

space = [\ \t]*
mspace = [\ \t\n\r]*

#[pub]
identificator -> String
	= [_a-zA-Z][0-9a-zA-Z_]* { String::from(match_str) }

#[pub]
sign -> i64
    = [\-\+]? space  { if match_str.starts_with("-") { -1 } else { 1 } }

unsigned -> i64
    = [0-9]+ {  match_str.parse().unwrap() }

#[pub]
signed -> i64
    = s:sign r:unsigned { s * r }

unsigned_decimal -> f64
    = [0-9]+ "." [0-9]+ { match_str.parse().unwrap() }

#[pub]
decimal -> f64
    = s:sign r:unsigned_decimal { (s as f64) * r }

#[pub]
exponential -> f64
    = b:decimal "e" e:signed { b * (10 as f64).powf(e as f64) }
    / b:signed "e" e:signed { (b as f64) * (10 as f64).powf(e as f64) }

unsigned_hex -> i64
    = "0x" [0-9A-Fa-f]+ { i64::from_str_radix(&match_str[2..], 16).unwrap() }

#[pub]
hexadecimal -> i64
    = s:sign u:unsigned_hex { s * u }

#[pub]
atom -> Atom
    = a:index { Atom::IndexOp(a) }
    / a:bracketOperation { Atom::Call(a) }
    / a:hexadecimal { Atom::Integer(a) }
    / a:identificator { Atom::Identificator(a) }
    / a:exponential { Atom::Float(a) }
    / a:decimal { Atom::Float(a) }
    / a:signed { Atom::Integer(a) }
    / a:_vector { Atom::Vector(a) }
    / a:_matrix { Atom::Matrix(a) }

// Matrix stuff

delim_vector
    = space ","? space

in_vector -> Vec<Atom>
    = atom ++ delim_vector

#[pub]
_vector -> Vec<Atom>
    = "[" mspace c:in_vector mspace "]" { c }

delim_matrix
    = space [;\n] mspace

in_matrix -> Vec<Vec<Atom>>
    = in_vector ++ delim_matrix

#[pub]
_matrix -> Vec<Vec<Atom>>
    = "[" mspace c:in_matrix mspace "]" { c }


// Brackets!

delim_bracket
    = space "," space

arg_list -> Vec<Atom>
    = atom ** delim_bracket

#[pub]
bracketOperation -> BracketOperator
 	= name:identificator space "(" space lst:arg_list space ")"
 	    { BracketOperator { name:Box::new(Atom::Identificator(String::from(name))), args:lst } }

delim_index
    = space ":" space

#[pub]
index -> IndexOperator
    = a1:atom delim_index a2:atom delim_index a3:atom
        { IndexOperator { from: Box::new(a1), delta: Box::new(a2), to:Box::new(a3), ..Default::default() } }
    / a1:atom delim_index a2:atom
        { IndexOperator { from: Box::new(a1), to:Box::new(a2), ..Default::default() } }
    / ":"
        { IndexOperator { full: true, ..Default::default() } }






// #[pub]
//
// #[pub]
// expression -> Expression
// 	= sum
// sum -> Expression
// 	= l:product "+" r:product { Expression::Sum(Box::new(l), Box::new(r)) }
// 	/ product
// product -> Expression
// 	= l:atom "*" r:atom { Expression::Product(Box::new(l), Box::new(r)) }
// 	/ atom
// atom -> Expression
// 	= number
// 	/ "(" v:sum ")" { v }
// number -> Expression
//	= [0-9]+ { Expression::Atom(Atom::Integer(match_str.parse().unwrap())) }
