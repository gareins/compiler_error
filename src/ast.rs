#[derive(Clone, PartialEq, Debug)]
pub struct BracketOperator {
    pub name: Box<Atom>, //Identificator
    pub args: Vec<Atom>
}

#[derive(Clone, PartialEq, Debug)]
pub struct IndexOperator {
    pub from: Box<Atom>,
    pub to: Box<Atom>,
    pub delta: Box<Atom>,
    pub full: bool
}

impl Default for IndexOperator {
    fn default() -> IndexOperator {
        IndexOperator{ from: Box::new(Atom::Integer(0)), to: Box::new(Atom::Integer(1)), delta: Box::new(Atom::Integer(1)), full: false }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum Atom {
	Identificator(String),
	Integer(i64),
	Float(f64),

	Matrix(Vec<Vec<Atom>>),
	Vector(Vec<Atom>),
	Call(BracketOperator),
    IndexOp(IndexOperator)
}

//#[derive(Clone, PartialEq, Debug)]
//pub enum Expression {
//	Atom(Atom),
//    Sum(Box<Expression>, Box<Expression>),
//    Product(Box<Expression>, Box<Expression>),
//}

//#[derive(Clone, PartialEq, Debug)]
//struct Assignment {
//	left: Vec<String>,
//	right: Atom
//}

//#[derive(Clone, PartialEq, Debug)]
//struct Command {
//	cmd: String,
//	args: Vec<Atom>,
//}

//#[derive(Clone, PartialEq, Debug)]
//pub enum Statement {
//	Assignment(Assignment),
//	Expression(Expression),
//}

//#[derive(Clone, PartialEq, Eq, Debug)]
//pub enum Block {
//	Main(Vec<Box<Expression>>),
//	Function(String, String, Vec<Box<Expression>>),
//	If(Vec<Box<Expression>>),
//	Elseif(Vec<Box<Expression>>),
//	Else(Vec<Box<Expression>>),
//	Expression
//}